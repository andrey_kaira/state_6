import 'package:flutter/material.dart';

import 'color_bloc.dart';

class MyApp extends StatelessWidget {
  ColorBloc colorBloc = ColorBloc();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('State Management Practice #4'),
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            AppBar(
              automaticallyImplyLeading: false,
            ),
            InkWell(
                child: Container(
                  color: Colors.red,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: () {
                  colorBloc.selColorRedStream.add(null);
                }),
            InkWell(
              child: Container(
                color: Colors.yellow,
                height: 100.0,
                margin: EdgeInsets.all(15.0),
              ),
              onTap: () => colorBloc.selColorYellowStream.add(null),
            ),
            InkWell(
              child: Container(
                color: Colors.green,
                height: 100.0,
                margin: EdgeInsets.all(15.0),
              ),
              onTap: () => colorBloc.selColorGreenStream.add(null),
            ),
          ],
        ),
      ),
      body: StreamBuilder<Color>(
        stream: colorBloc.selColor,
        builder: (ctx, snapshot) => Container(
          color: snapshot.data,
        ),
      ),
    );
  }
}
