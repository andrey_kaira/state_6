import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ColorBloc {
  Color _selColor;

  ColorBloc() {
    _selColor = Colors.black;
    _actionController.stream.listen(_selColorRedStream);
    _actionControllerGreen.stream.listen(_selColorGreenStream);
    _actionControllerYellow.stream.listen(_selColorYellowStream);
   //_actionController.stream.listen(_selColorGreenStream);
    //_actionController.stream.listen(_selColorYellowStream);
  }

  final _colorStream = BehaviorSubject<Color>.seeded(Colors.black);

  Stream get selColor => _colorStream.stream;
  Sink get _addValue => _colorStream.sink;

  StreamController _actionController = StreamController();
  StreamController _actionControllerGreen = StreamController();
  StreamController _actionControllerYellow = StreamController();
  StreamSink get selColorRedStream => _actionController.sink;
  StreamSink get selColorGreenStream => _actionControllerGreen.sink;
  StreamSink get selColorYellowStream => _actionControllerYellow.sink;
  //StreamSink get selColorGreenStream => _actionController.sink;
 // StreamSink get selColorYellowStream => _actionController.sink;

  void _selColorRedStream(data) {
    print('Test');
    _selColor = Colors.red;
    _addValue.add(_selColor);
  }

  void _selColorGreenStream(data) {
    _selColor = Colors.green;
    _addValue.add(_selColor);
  }

  void _selColorYellowStream(data) {
    _selColor = Colors.yellow;
    _addValue.add(_selColor);
  }

  void dispose() {
    _colorStream.close();
    _actionController.close();
  }
}